use db_hanc;

CREATE DEFINER=`root`@`%` PROCEDURE `SaveLink`( in url varchar(245), in description varchar(45), in tag varchar(45) )
BEGIN
	SET @tagToSave = tag;
    
	if exists( select 1 from link where link_name = url ) then
    begin
		update link set link_name=url, link_description=description where link.link_name=url;
        if not exists( select 1 from metatag where tag_name = tag ) then
			insert into metatag values ( tag );
		end if;        
	end;
	else
		insert into link (link_name, link_description) values( url, description );
        if not exists( select 1 from metatag where tag_name = tag ) then
			insert into metatag values ( tag );
		end if;
        insert into linkToMetatag ( tag_name, link_id ) 
			select @tagToSave, link_id from link where link.link_name = url;
	end if;
END